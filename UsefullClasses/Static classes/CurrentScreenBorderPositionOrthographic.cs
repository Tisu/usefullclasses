﻿using UnityEngine;
using System.Collections;
using System;

namespace Tisu
{
    /// <summary>
    /// Position of screen borders only works properly when main camera has projection:orthographic
    /// </summary>
    public static class CurrentScreenBorderPositionOrthographic
    {
        private static Vector2 m_screen_size = Vector2.zero;

        /// <summary>
        /// return actuall screen border right(x) and up (y)
        /// </summary>
        public static Vector2 m_screen_right_up_border
        {
            get
            {
                return UpdateScreenBordersRightUp();
            }
        }
        /// <summary>
        /// return actuall screen boder left(x) and down (y)
        /// </summary>
        public static Vector2 m_screen_left_down_border
        {
            get
            {
                return UpdateScreenBordersLeftDown();
            }
        }
        /// <summary>
        /// X coordinate scrren borders x->left y->right
        /// </summary>
        public static Vector2 m_screen_x_borders
        {
            get
            {
                return UpdateScreenXBorders();
            }
        }
        /// <summary>
        /// Y coodrinate screen borders x-> down y-up
        /// </summary>
        public static Vector2 m_screen_y_border
        {
            get
            {
                return UpdateScreenYBorders();
            }
        }
        /// <summary>
        /// X coordinate scrren borders x->left y->right
        /// </summary>
        public static Vector2 UpdateScreenXBorders()
        {
            var x_border = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0))) * 0.5f;
            return new Vector2(Camera.main.transform.position.x - x_border, Camera.main.transform.position.x + x_border);
        }
        /// <summary>
        /// Y coodrinate screen borders x-> down y-up
        /// </summary>
        public static Vector2 UpdateScreenYBorders()
        {
            var y_border = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height))) * 0.5f;
            return new Vector2(Camera.main.transform.position.y - y_border, Camera.main.transform.position.y + y_border);
        }
        /// <summary>
        /// return actuall screen border left(x) and down (y)
        /// </summary>
        public static Vector2 UpdateScreenBordersRightUp()
        {
            m_screen_size.x = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0))) * 0.5f;
            m_screen_size.y = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height))) * 0.5f;
            return (Vector2)Camera.main.transform.position + m_screen_size;
        }
        /// <summary>
        /// return actuall screen boder right(x) and up (y)
        /// </summary>
        public static Vector2 UpdateScreenBordersLeftDown()
        {
            m_screen_size.x = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0))) * 0.5f;
            m_screen_size.y = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height))) * 0.5f;
            return (Vector2)Camera.main.transform.position - m_screen_size;
        }
    }
}
