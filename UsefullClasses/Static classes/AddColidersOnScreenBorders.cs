﻿using System.Collections.Generic;
using UnityEngine;

namespace Tisu
{
    /// <summary>
    /// Adding Colliders on border of current screen
    /// </summary>
    public static class AddColidersOnScreenBorders
    {
        static float m_collider_thickness = 10f;
        static float m_position_z = 0f;
        static Dictionary<string, Transform> m_colliders = new Dictionary<string, Transform>();
        const float OVERLAPPING_AREA = 6F;
        /// <summary>
        /// Add 4 colladers 
        /// </summary>
        /// <param name="a_parent">parent of created colliders (gameobjects)</param>
        public static void AddColliders(GameObject a_parent)
        {
            m_colliders.Add("Top", new GameObject().transform);
            m_colliders.Add("Bottom", new GameObject().transform);
            m_colliders.Add("Right", new GameObject().transform);
            m_colliders.Add("Left", new GameObject().transform);
            Vector2 main_camera_pos = Camera.main.transform.position;

            foreach (KeyValuePair<string, Transform> valPair in m_colliders)
            {
                valPair.Value.gameObject.AddComponent<BoxCollider2D>();
                valPair.Value.name = valPair.Key + "Collider";
                valPair.Value.parent = a_parent.transform;

                if (valPair.Key == "Left" || valPair.Key == "Right")
                    valPair.Value.localScale = new Vector3(m_collider_thickness, main_camera_pos.y * 2 + OVERLAPPING_AREA, m_collider_thickness);
                else if (valPair.Key == "Bottom" || valPair.Key == "Top")
                {
                    valPair.Value.localScale = new Vector3(main_camera_pos.x * 2 + OVERLAPPING_AREA, m_collider_thickness, m_collider_thickness);
                }

            }
            Vector2 screen_border_x = CurrentScreenBorderPositionOrthographic.m_screen_x_borders;
            Vector2 screen_border_y = CurrentScreenBorderPositionOrthographic.m_screen_y_border;

            m_colliders["Right"].position = new Vector3(screen_border_x.y + (m_colliders["Right"].localScale.x * 0.5f), main_camera_pos.y, m_position_z);
            m_colliders["Left"].position = new Vector3(screen_border_x.x - (m_colliders["Left"].localScale.x * 0.5f), main_camera_pos.y, m_position_z);
            m_colliders["Top"].position = new Vector3(main_camera_pos.x, screen_border_y.y + (m_colliders["Top"].localScale.y * 0.5f), m_position_z);
            m_colliders["Bottom"].position = new Vector3(main_camera_pos.x, screen_border_y.x - (m_colliders["Bottom"].localScale.y * 0.5f), m_position_z);
            if (a_parent != null)
            {
                foreach (KeyValuePair<string, Transform> valPair in m_colliders)
                {
                    valPair.Value.transform.parent = a_parent.transform;
                }
            }
        }
        /// <summary>
        /// Configure Parameters of created colliders
        /// </summary>
        /// <param name="a_collider_thickness"></param>
        /// <param name="a_position_z"></param>
        public static void Configure(float a_collider_thickness, float a_position_z)
        {
            m_position_z = a_position_z;
            m_collider_thickness = a_collider_thickness;
        }
    }
}
