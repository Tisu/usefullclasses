﻿namespace Tisu
{
    using UnityEngine;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.IO;
    /// <summary>
    /// Filestatus 
    /// </summary>
    public enum eFileStatus
    {
        /// <summary>
        /// File doesnt exist when loading
        /// </summary>
        file_doesnt_exist,
        /// <summary>
        /// Path empty
        /// </summary>
        argument_path_is_empty,
        /// <summary>
        /// member path is empty
        /// </summary>
        member_path_is_empty,
        /// <summary>
        /// path empty
        /// </summary>
        path_is_empty,
        /// <summary>
        /// Serialize was not successfull
        /// </summary>
        serialize_wasnt_successful,
        /// <summary>
        /// Derialize was not successfull
        /// </summary>
        deserialized_wasnt_successful,
        /// <summary>
        /// Derialize was successfull
        /// </summary>
        deserialized_successful,
        /// <summary>
        /// METHOD SAVE RETURN THIS IF SUCCESSFULLY SAVED
        /// </summary>
        save_successful,
        /// <summary>
        /// METHOD LOAD RETURN THIS IF SUCCESSFULLY LOADED
        /// </summary>
        load_successful,
        /// <summary>
        /// Save failed
        /// </summary>
        save_failed,
        /// <summary>
        /// load failed
        /// </summary>
        load_failed
    }
    /// <summary>
    /// Possible mistakes:
    /// - generic T class/struct isn't the same in load and save
    /// - You try to serialize nonserialize variable
    /// - check if you have [Serializable] attribute above T 
    /// 
    /// UNITY:
    /// - Neither Vetor3 nor Transfrom is NOT erializable
    /// </summary>
    public class BinarySerializer
    {
        private readonly string FULL_FILE_PATH;
        /// <summary>
        /// Provides information about thrown exeptions
        /// </summary>
        public BinarySerializer()
        {
            FULL_FILE_PATH = null;
        }
        /// <summary>
        /// Dont know if Application.persistentDataPath works on other apps than Unity. Use a_path for other.
        /// </summary>
        /// <param name="a_string"></param>
        public BinarySerializer(string a_string)
        {
            if (a_string.Contains("/"))
            {
                FULL_FILE_PATH = a_string;
            }
            else
            {
                FULL_FILE_PATH = Application.persistentDataPath + "/" + a_string;
            }
        }
        /// <summary>
        /// Saving [Serializable] data to a_path or FILE_NAME_WITH_EXTENSION
        /// </summary>
        /// <typeparam name="T">object type which contains data</typeparam>
        /// <param name="a_serialize_object">ref to the object from which data is saved </param>
        /// <param name="a_path">optional parameter path to save file WITH EXTENSION , when null constructor path is taken into account</param>
        /// <returns>eFileStatus.file_save_successful id successfull, otherwise see efilestatus</returns>
        public eFileStatus Save<T>(ref T a_serialize_object, string a_path = null)
        {
            BinaryFormatter binary_formater = new BinaryFormatter();
            FileStream file;

            if (string.IsNullOrEmpty(a_path))
            {
                if (string.IsNullOrEmpty(FULL_FILE_PATH))
                {
                    System.Diagnostics.Debug.Assert(false, "FILE_NAME_WITH_EXTENSION is empty");
                    Logger.LogError("FILE_NAME_WITH_EXTENSION is empty");
                    return eFileStatus.member_path_is_empty;
                }
                else
                {
                    file = File.Create(FULL_FILE_PATH);
                }
            }
            else
            {
                file = File.Create(a_path);
            }

            bool exeption_thrown = false;
            try
            {
                binary_formater.Serialize(file, a_serialize_object);
            }
            catch (System.Exception e)
            {
                Logger.LogError(e.Message);
                exeption_thrown = true;
            }
            finally
            {
                file.Close();
            }
            if (exeption_thrown)
            {
                return eFileStatus.serialize_wasnt_successful;
            }
            else
            {
                return eFileStatus.save_successful;
            }
        }
        /// <summary>
        /// Load data from a_path if not empty, then from FILE_NAME_WITH_EXTENSION to a_return_object
        /// </summary>
        /// <typeparam name="T">object type, data container</typeparam>
        /// <param name="a_return_object">ref to data container</param>
        /// <param name="a_path">optional parameter file path WITH EXTENSION</param>
        /// <returns>eFileStatus.deserialized_successfull if successfull</returns>
        public eFileStatus Load<T>(ref T a_return_object, string a_path = null)
        {
            if (!string.IsNullOrEmpty(a_path))
            {
                if (LoadHelp<T>(ref a_return_object, a_path) == eFileStatus.deserialized_successful)
                {
                    return eFileStatus.load_successful;
                }
                else
                {
                    return eFileStatus.load_failed;
                }
            }
            else if (!string.IsNullOrEmpty(FULL_FILE_PATH))
            {
                if (LoadHelp<T>(ref a_return_object, FULL_FILE_PATH) == eFileStatus.deserialized_successful)
                {
                    return eFileStatus.load_successful;
                }
                else
                {
                    return eFileStatus.load_failed;
                }
            }
            else
            {
                Logger.LogError("FILE_NAME_WITH_EXTENSION and a_path are empty");
                System.Diagnostics.Debug.Assert(false, "FILE_NAME_WITH_EXTENSION and a_path are empty");
                return eFileStatus.path_is_empty;
            }
        }
        private eFileStatus LoadHelp<T>(ref T a_load_object, string a_path)
        {
            if (File.Exists(a_path))
            {
                bool exeption_thrown = false;
                BinaryFormatter binary_formater = new BinaryFormatter();
                FileStream file = new FileStream(a_path, FileMode.Open);
                try
                {
                    a_load_object = (T)binary_formater.Deserialize(file);
                }
                catch (System.Exception e)
                {
                    Logger.LogError(e.ToString());
                    exeption_thrown = true;
                }
                finally
                {
                    file.Close();
                }
                if (exeption_thrown)
                {
                    return eFileStatus.deserialized_wasnt_successful;
                }
                else
                {
                    return eFileStatus.deserialized_successful;
                }
            }
            else
            {
                return eFileStatus.file_doesnt_exist;
            }
        }
    }
}
