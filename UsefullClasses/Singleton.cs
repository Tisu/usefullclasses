﻿namespace Tisu
{
    /// <summary>
    /// Singleton of class T which has parametreless constructor
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class Singleton<T> where T : class, new()
    {
        private static T m_instanceP = null;
        private static object m_lock = new object();
        /// <summary>
        /// Instance of singletone
        /// </summary>
        public static T m_instance
        {
            get
            {
                if (m_instanceP == null)
                {
                    lock (m_lock)
                    {
                        if (m_instanceP == null)
                        {
                            m_instanceP = new T();
                        }
                    }
                }
                return m_instanceP;
            }
        }
    }
}
