﻿namespace Tisu
{
    /// <summary>
    /// Log mode/ level
    /// </summary>
    public enum eLog
    {
        /// <summary>
        /// Node when dont log 
        /// </summary>
        NONE,
        /// <summary>
        /// Debug log 
        /// </summary>
        Debug_Mode_1,
        /// <summary>
        /// Debug log 
        /// </summary>
        Debug_Mode_2,
        /// <summary>
        /// Debug log 
        /// </summary>
        Debug_Mode_3,
        /// <summary>
        /// Warning log 
        /// </summary>
        Warning,
        /// <summary>
        /// Error log 
        /// </summary>
        Error,
    }
    /// <summary>
    /// Logger Configuration structure
    /// </summary>
    public struct sLoggerConfiguration
    {
        /// <summary>
        /// log level
        /// </summary>
        public readonly eLog m_log_level;
        /// <summary>
        /// If log to file
        /// </summary>
        public readonly bool m_if_log_to_file;
        /// <summary>
        /// How many days store logs
        /// </summary>
        public readonly eFileTimeSpan m_file_delete_timespan;
        /// <summary>
        /// When using LogDebug
        /// </summary>
        public readonly eLog m_debug_log_level;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="a_log_level">log level</param>
        /// <param name="a_log_to_file">If log to file</param>
        /// <param name="a_file_time_span">How many days store logs</param>
        /// <param name="a_debug_log_level">When using LogDebug</param>
        public sLoggerConfiguration(eLog a_log_level, bool a_log_to_file, eFileTimeSpan a_file_time_span, eLog a_debug_log_level = eLog.Debug_Mode_1)
        {
            m_log_level = a_log_level;
            m_if_log_to_file = a_log_to_file;
            m_file_delete_timespan = a_file_time_span;
            m_debug_log_level = a_debug_log_level;
        }
    }
    /// <summary>
    /// LOGGER WORKS with DEBUG and TISU_LOGGER macros only
    /// </summary>   
    public static class Logger
    {
        #region Logger const definitions      
        const string CALLER_SEPARATOR = " : ";
        const string TARGET_SEPARATOR = " target: ";
        const uint MAX_SAME_MESSAGE = 200;
        #endregion

        #region LOGGING LEVEL / CONFIGURATION
        static readonly string FOLDER_PATH = System.IO.Directory.GetCurrentDirectory() + @"/LOGS/";
        static eLog m_log_level = eLog.Debug_Mode_1;
        static bool m_if_log_to_file = true;
        static eFileTimeSpan m_file_deleter_time_span = eFileTimeSpan.ThreeDays;
        static eLog m_debug_log_level = eLog.Debug_Mode_1;
        static readonly string[] m_delete_file_extension = { ".txt", ".meta" };
        #endregion

        #region members
        static bool m_already_configured = false;
        static readonly object m_lock = new object();
        static readonly FileLogger m_file_logger;
        static string m_last_message;
        static uint m_last_message_count;
        #endregion
        #region constructors /destructor /confuguration method
        /// <summary>
        /// Default constructor. Configuration from Logger.cs
        /// </summary>

        static Logger()
        {
            if (m_if_log_to_file)
            {
                m_file_logger = new FileLogger(FOLDER_PATH);
            }
            FileDeleter.DeleteOldFiles(FOLDER_PATH, m_file_deleter_time_span, m_delete_file_extension);
        }
        /// <summary>
        /// Configuration of Logger
        /// </summary>
        /// <param name="a_logger_configuration">Loger configuration</param>
        /// <param name="a_delete_old_files">If delete old files</param>       
        [System.Diagnostics.Conditional("DEBUG")]
        public static void ConfigureLogger(sLoggerConfiguration a_logger_configuration, bool a_delete_old_files = false)
        {
            if (m_already_configured)
            {
                return;
            }
            m_file_deleter_time_span = a_logger_configuration.m_file_delete_timespan;
            m_if_log_to_file = a_logger_configuration.m_if_log_to_file;
            m_log_level = a_logger_configuration.m_log_level;
            m_debug_log_level = a_logger_configuration.m_debug_log_level;
            m_already_configured = true;
            if (a_delete_old_files)
            {
                FileDeleter.DeleteOldFiles(FOLDER_PATH, m_file_deleter_time_span);
            }
        }
        /// <summary>
        /// Changing current log level
        /// </summary>
        /// <param name="a_log_level">log level</param>       
        [System.Diagnostics.Conditional("DEBUG")]
        public static void ChangeLogLevel(eLog a_log_level)
        {
            m_log_level = a_log_level;
        }
        #endregion
        /// <summary>
        /// Basic Log which log a_message and display in unity (Logs accordingly to configuration)
        /// </summary>
        /// <param name="a_log_level">Log level if lower ignore logging</param>
        /// <param name="a_message">Message to log</param>       
        [System.Diagnostics.Conditional("DEBUG")]
        public static void Log(eLog a_log_level, string a_message)
        {
            if (a_log_level < m_log_level)
            {
                return;
            }
            if (m_last_message == a_message)
            {
                m_last_message_count++;
                if (m_last_message_count > MAX_SAME_MESSAGE)
                {
                    return;
                }
            }
            else
            {
                m_last_message_count = 0;
            }

            m_last_message = a_message;
            lock (m_lock)
            {
                LogToFile(ref a_message);
                LogToUnity(a_log_level, ref a_message);
            }
        }
        /// <summary>
        /// Logging accordingly to a_log_level message + caller (e.g a_caller : a_message)
        /// </summary>
        /// <param name="a_log_level">Log level if lower ignore logging</param>
        /// <param name="a_caller">GameObject which calls Log</param>
        /// <param name="a_message">Message to log</param>
        [System.Diagnostics.Conditional("DEBUG")]       
        public static void Log(eLog a_log_level, UnityEngine.GameObject a_caller, string a_message)
        {
            Log(a_log_level, a_caller + CALLER_SEPARATOR + a_message);
        }
        /// <summary>
        ///  Logging accordingly to a_log_level message + target (e.g a_message target: a_target)
        /// </summary>
        /// <param name="a_log_level">Log level if lower ignore logging</param>
        /// <param name="a_message">Message to log</param>
        /// <param name="a_target">Target when GameObject is not from caller instance</param>
        [System.Diagnostics.Conditional("DEBUG")]       
        public static void Log(eLog a_log_level, string a_message, UnityEngine.GameObject a_target)
        {
            Log(a_log_level, a_message + TARGET_SEPARATOR + a_target);
        }
        /// <summary>
        /// Logging Error
        /// </summary>
        /// <param name="a_message">Message to log</param>       
        [System.Diagnostics.Conditional("DEBUG")]
        public static void LogError(string a_message)
        {
            Log(eLog.Error, a_message);
        }
        /// <summary>
        /// Logging Error
        /// </summary>
        /// <param name="a_condition">if true log</param>
        /// <param name="a_message">Message to log</param>        
        [System.Diagnostics.Conditional("DEBUG")]
        public static void LogError(bool a_condition, string a_message)
        {
            if (a_condition)
            {
                Log(eLog.Error, a_message);
            }
        }

        /// <summary>
        /// Log Warning
        /// </summary>
        /// <param name="a_message">Message to log</param>       
        [System.Diagnostics.Conditional("DEBUG")]
        public static void LogWarning(string a_message)
        {
            Log(eLog.Warning, a_message);
        }
        /// <summary>
        /// Log Warning
        /// </summary>
        /// <param name="a_condition">if true log</param>
        /// <param name="a_message">Message to log</param>       
        [System.Diagnostics.Conditional("DEBUG")]
        public static void LogWarning(bool a_condition, string a_message)
        {
            if (a_condition)
            {
                Log(eLog.Warning, a_message);
            }
        }

        /// <summary>
        /// Log debug according to set m_debug_log_level
        /// </summary>
        /// <param name="a_message">Message to log</param>       
        [System.Diagnostics.Conditional("DEBUG")]
        public static void LogDebug(string a_message)
        {
            Log(m_debug_log_level, a_message);
        }
        /// <summary>
        /// Log debug according to set m_debug_log_level
        /// </summary>
        /// <param name="a_condition">if true log</param>
        /// <param name="a_message">Message to log</param>        
        [System.Diagnostics.Conditional("DEBUG")]
        public static void LogDebug(bool a_condition, string a_message)
        {
            if (a_condition)
            {
                Log(m_debug_log_level, a_message);
            }
        }
        /// <summary>
        /// Logging to file
        /// </summary>
        /// <param name="a_message">Message to log in file</param>  
        [System.Diagnostics.Conditional("DEBUG")]
        private static void LogToFile(ref string a_message)
        {
            if (m_if_log_to_file && m_file_logger != null)
            {
                m_file_logger.LogToFile(ref a_message);
            }
        }
        [System.Diagnostics.Conditional("DEBUG")]
        [System.Diagnostics.Conditional("UNITY")]
        private static void LogToUnity(eLog a_log_level, ref string a_message)
        {
            switch (a_log_level)
            {
                case eLog.Debug_Mode_1:
                case eLog.Debug_Mode_2:
                case eLog.Debug_Mode_3:
                    {
                        UnityEngine.Debug.Log(a_message);
                        break;
                    }
                case eLog.Warning:
                    {
                        UnityEngine.Debug.LogWarning(a_message);
                        break;
                    }
                case eLog.Error:
                    {
                        UnityEngine.Debug.LogError(a_message);
                        break;
                    }
                default:
                    {
                        System.Diagnostics.Debug.Assert(false, "Add new log level to method!");
                        string log_string = " >>>>>>> Add new log level to method! <<<<<<<<<<<<";
                        LogToFile(ref log_string);
                        break;
                    }
            }
        }
    }
}
