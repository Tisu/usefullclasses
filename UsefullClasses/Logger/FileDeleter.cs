﻿using System;
using System.IO;

namespace Tisu
{
    /// <summary>
    /// How many days store logs
    /// </summary>
    public enum eFileTimeSpan
    {
        /// <summary>
        /// 1 Day
        /// </summary>
        OneDay = 1,
        /// <summary>
        /// 2 Days
        /// </summary>
        TwoDays,
        /// <summary>
        /// 3 Days
        /// </summary>
        ThreeDays,
        /// <summary>
        /// 4 Days
        /// </summary>
        FourDays,
        /// <summary>
        /// 5 Days
        /// </summary>
        FiveDays,
        /// <summary>
        /// 6 Days
        /// </summary>
        SixDays,
        /// <summary>
        /// 7 Days
        /// </summary>
        SevenDays = 7,
        /// <summary>
        /// 7 Days
        /// </summary>
        Week = 7,
        /// <summary>
        /// Month 30 days
        /// </summary>
        Month = 30,
    }
    /// <summary>
    /// Possible folder state
    /// </summary>
    public enum eFolderState
    {
        /// <summary>
        /// Ok
        /// </summary>
        OK,
        /// <summary>
        /// does not exist
        /// </summary>
        NotExist,
        /// <summary>
        /// Wrong path of folder
        /// </summary>
        WrongPath
    }
    /// <summary>
    /// Class to delete old files from folder
    /// </summary>
    public static class FileDeleter
    {
        /// <returns></returns>
        /// <summary>
        /// Deleting old files
        /// </summary>
        /// <param name="a_target_directory">target directory to check for files</param>
        /// <param name="a_file_time_span">How old files should be deleted</param>
        /// <param name="a_file_extension_to_delete">file extensions to delete, if not given delete all files</param>
        /// <returns></returns>
        public static eFolderState DeleteOldFiles(string a_target_directory, eFileTimeSpan a_file_time_span, string[] a_file_extension_to_delete = null)
        {
            if (String.IsNullOrEmpty(a_target_directory))
            {
                return eFolderState.WrongPath;
            }
            if (!Directory.Exists(a_target_directory))
            {
                return eFolderState.NotExist;
            }
            DateTime file_creation_time;
            TimeSpan file_time_difference;

            string[] file_names = Directory.GetFiles(a_target_directory);
            string[] buffor;

            foreach (var file_path in file_names)
            {

                buffor = Path.GetFileName(file_path).Split('_','.');
                if (buffor.Length == 0)
                {
                    continue;
                }


                if (!ParseToDataTime(buffor, out file_creation_time))
                {
                    continue;
                }
                file_time_difference = DateTime.Now.Subtract(file_creation_time);

                if (file_time_difference.TotalDays > (double)a_file_time_span)
                {
                    if (a_file_extension_to_delete.Length > 0)
                    {

                        string file_extension = Path.GetExtension(file_path);

                        foreach (var extension in a_file_extension_to_delete)
                        {
                            if (file_extension == extension)
                            {
                                File.Delete(file_path);
                                break;
                            }
                        }
                    }
                    else
                    {
                        File.Delete(file_path);
                    }
                }
            }
            return eFolderState.OK;
        }

        private static bool ParseToDataTime(string[] a_buffor, out DateTime a_date)
        {
            int year = 0;
            int month = 0;
            int day = 0;
            a_date = new DateTime();
            if (!int.TryParse(a_buffor[0], out day))
            {
                return false;
            }
            if (!int.TryParse(a_buffor[1], out month))
            {
                return false;
            }
            if (!int.TryParse(a_buffor[2], out year))
            {
                return false;
            }
            a_date = new DateTime(year, month, day);
            return true;
        }
    }
}
