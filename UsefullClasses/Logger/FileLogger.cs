﻿using System;
using System.Diagnostics;
using System.IO;

namespace Tisu
{
    /// <summary>
    /// File Logger
    /// </summary>
    public class FileLogger : IDisposable
    {
        #region LOGGING CONFIGURATION
        const string LOG_SEPARATOR = " >> ";
        const string FILE_EXTENSION = ".txt";
        const string FILE_NAME_PATTERN = "dd_MM_yyyy";
        const string FILE_LOG_DATE_PATTERN = "HH:mm:ss";
        readonly string FIRST_LOG_PATTERN = Environment.NewLine + "||||||||||||||||||||||||||||||||| FILE OPENED ||||||||||||||||||||||||||||||||| " + Environment.NewLine;
        const string LOG_FOLDER = @"\logs\";

        readonly string FOLDER_PATH = Directory.GetCurrentDirectory() + LOG_FOLDER;
        #endregion
        readonly StreamWriter m_file;
        /// <summary>
        /// Constructor
        /// </summary>
        public FileLogger()
        {
            if (!Directory.Exists(FOLDER_PATH))
            {
                Directory.CreateDirectory(FOLDER_PATH);
            }
            string log_file_name = DateTime.Today.ToString(FILE_NAME_PATTERN);
            m_file = new StreamWriter(FOLDER_PATH + log_file_name + FILE_EXTENSION, true);
            m_file.AutoFlush = true;
            m_file.Write(FIRST_LOG_PATTERN);
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="a_folder_path">path to logging folder NOTE: without file name and extension!</param>
        public FileLogger(string a_folder_path)
        {
            if (!String.IsNullOrEmpty(a_folder_path))
            {
                FOLDER_PATH = a_folder_path;
                if (!Directory.Exists(a_folder_path))
                {
                    Directory.CreateDirectory(FOLDER_PATH);
                }
            }
            else if (!Directory.Exists(FOLDER_PATH))
            {
                Directory.CreateDirectory(FOLDER_PATH);
            }
            string log_file_name = DateTime.Today.ToString(FILE_NAME_PATTERN);
            m_file = new StreamWriter(FOLDER_PATH + log_file_name + FILE_EXTENSION, true);
            m_file.AutoFlush = true;
            m_file.Write(FIRST_LOG_PATTERN);
        }
        /// <summary>
        /// destructor
        /// </summary>
        ~FileLogger()
        {

        }
        /// <summary>
        /// Log to file opened in constructor
        /// </summary>
        /// <param name="a_message">message to log</param>
        public void LogToFile(ref string a_message)
        {
            if (String.IsNullOrEmpty(a_message))
            {
                return;
            }
            m_file.WriteLine(DateTime.Now.ToString(FILE_LOG_DATE_PATTERN) + CallStack() + LOG_SEPARATOR + a_message);
        }
        /// <summary>
        /// Log to file without ref
        /// </summary>
        /// <param name="a_message"></param>
        public void LogToFile(string a_message)
        {
            if (String.IsNullOrEmpty(a_message))
            {
                return;
            }
            m_file.WriteLine(DateTime.Now.ToString(FILE_LOG_DATE_PATTERN) + CallStack() + LOG_SEPARATOR + a_message);
        }
        /// <summary>
        /// Dispose of resources
        /// </summary>
        public void Dispose()
        {
            if (m_file != null )
            {
                m_file.Dispose();
            }
        }
        private string CallStack()
        {
            var stack = new StackFrame(5, true);
            int line_number = stack.GetFileLineNumber();
            var caller_name = " " + stack.GetMethod().DeclaringType + " line: " + line_number;
            return caller_name;
        }
    }
}
